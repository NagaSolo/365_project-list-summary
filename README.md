### Overview
- summary and list to each day initiated projects/solutions/products

### Workflow
- create `index.html`, resource is from here: https://www.w3schools.com/howto/howto_js_todolist.asp

- create `style.css` resource is from: https://www.w3schools.com/howto/howto_js_todolist.asp

- create `script.js` resource is from: https://www.w3schools.com/howto/howto_js_todolist.asp

- include `src="script.js"` into `script` tag of html `body`

- include `style.css` into `link` tag of html `head` - external css

- update favicon to page 

### Progress

- script to add content automatically, with option date -> `WIP`

- verification and validation page before being able to add content -> `NEXT`

- list order follows dates sequentially -> `NEXT`

- localstorage API -> `NEXT`